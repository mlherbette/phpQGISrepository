FROM webdevops/php-apache:7.4

WORKDIR /app

# see file .dockerignore for files or folders that won't be part of the container, but set up as volumes
COPY ./ ./

EXPOSE 8082
<?php
    $default_min_version="3.0"; // If there is no minimum version set in the plugin zip file, this setting is used as minimum QGIS version for the plugin.
    $count_downloads = false; // Should phpQGISrepository keep stats of how many times a plugin is downloaded? Options are true or false (without quotes).
?>
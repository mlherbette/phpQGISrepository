<?php
    $this_dir = dirname(__FILE__);
    $plugin = isset($_GET['plugin']) ? $_GET['plugin'] : '';

    if($plugin == ""){
        echo("<h1>No plugin name given</h1>");
        die();
    }

    $file = $this_dir.'/downloads/'.$plugin.'.zip';

    if (!file_exists($file)) {
        echo("<h1>The zip-file for plugin '$plugin' does not exist</h1>");
        die();
    }
    

    $statsfile_url = $this_dir.'/stats/'.$plugin.'.txt';
    if (!file_exists($statsfile_url)) {
        $statsfile = fopen($statsfile_url, "w");
        fwrite($statsfile, "1");
    }
    else{
        $statsfile_content = trim(fgets(fopen($statsfile_url, 'r')));
        preg_match('/\d+/', $statsfile_content, $statsfile_content_integer);
        $previous_stat = $statsfile_content_integer[0];
        if($previous_stat==""){$previous_stat = 0;}
        $new_stat = $previous_stat+1;
        $statsfile = fopen($statsfile_url, "w");
        fwrite($statsfile, $new_stat);
    }
    fclose($statsfile);

    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary"); 
    header("Content-disposition: attachment; filename=\"" . basename($file) . "\""); 
    ob_clean(); 
    flush();
    readfile($file); 
    exit();
?>